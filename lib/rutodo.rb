require 'yaml'
require 'todo-txt'
require 'terminal-table'
require "rutodo/version"
require "rutodo/configuration"
require "rutodo/interface"
require "rutodo/cli"

# Rutodo - A todo.txt CLI/TUI in Ruby.
# @author Jan Lindblom <janlindblom@fastmail.fm>
module Rutodo

end
