require 'rutodo/configuration'
require "highline"
require 'todo-txt'
require 'os'

module Rutodo
  # Rutodo interface class, the interface between todo.txt file and the CLI/TUI.
  # @since 0.0.1
  # @author Jan Lindblom <janlindblom@fastmail.fm>
  class Interface
    # All tasks stored in the todo.txt file.
    # @return [Array] list of tasks in the todo.txt file.
    attr_reader :todos

    # Create a new instance of {Interface}.
    def initialize
      @configuration = Rutodo::Configuration.new
      @configuration.load!

      @todos = Todo::List.new @configuration.todos
    end

    # Generate an ASCII table with the tasks in the todo.txt file.
    #
    # @return [Terminal::Table] table object with all tasks.
    def todos_table
      rows = []
      table = Terminal::Table.new(headings: table_headings, rows: rows)
      @todos.each_with_index do |todo, index|
        table.add_row [
          index,
          todo.done? ? checkmark : "",
          pretty_priority(todo.priority),
          pretty_text(todo.text, todo.projects, todo.contexts),
          todo.tags.has_key?(:due) ? todo.tags[:due] : ""
        ]
      end
      table
    end

    private

    def table_headings
      headings = ["#", "D", "P", "Text", "Due date"]
      if @configuration.colours
        headings = headings.map { |heading|
          HighLine.color(heading, HighLine::BRIGHT_WHITE, HighLine::BOLD) }
      end
      headings
    end

    def pretty_text(text, projects, contexts)
      "#{text} #{pretty_list projects, :projects} #{pretty_list contexts, :contexts}"
    end

    def pretty_list(list, what_list)
      joined_list = list.join " "
      if @configuration.colours
        case what_list
        when :projects
          colour = HighLine::MAGENTA
        when :contexts
          colour = HighLine::CYAN
        else
          colour = HighLine::NONE
        end
        joined_list = HighLine.color(joined_list, colour)
      end

    end

    def pretty_priority(priority)
      prio = priority
      if @configuration.colours
        colourings = coloured_priorities
        prio = HighLine.color(prio, HighLine::BOLD, colourings[prio].code)
      end
      prio
    end

    def checkmark
      mark = "x"
      mark = "✔" unless OS.windows?
      if @configuration.colours
        mark = HighLine.color(mark, HighLine::GREEN, HighLine::BOLD)
      end
      mark
    end

    def priorities
      (10...36).map{ |i| i.to_s(36).upcase }
    end

    def shades(number_of_shades=26)
      red = 255
      green = 0
      stepSize = 512 / (number_of_shades - 2)
      shades = []
      while green < 255
        shades << HighLine::Style.rgb(red, green, 0)
        green += stepSize
        green = 255 if green > 255
      end

      while red > 0
        red -= stepSize
        red = 0 if red < 0
        shades << HighLine::Style.rgb(red, green, 0)
      end
      shades
    end

    def coloured_priorities
      data = {}
      number = priorities.size
      all_shades = shades number
      priorities.each_with_index do |p, i|
        data[priorities[i]] = all_shades[i]
      end
      data
    end

  end
end