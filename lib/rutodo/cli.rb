require 'rubygems'
require 'commander'
require 'rutodo'

module Rutodo
  # The Rutodo CLI/TUI.
  # @since 0.0.1
  # @author Jan Lindblom <janlindblom@fastmail.fm>
  class Cli
    include Commander::Methods

    # Run the CLI/TUI.
    def run
      @configuration = Rutodo::Configuration.new
      @interface = Rutodo::Interface.new

      program :name, 'rutodo'
      program :version, Rutodo::VERSION
      program :description, 'A todo.txt CLI.'
      program :help, 'Author', 'Jan Lindblom <janlindblom@fastmail.fm>'

      global_option '-c', '--config FILE', 'Load custom configuration from named file.' do |file|
        # Do something
      end

      command :interactive do |c|
        c.syntax = 'rutodo interactive'
        c.summary = ''
        c.description = ''
        c.example 'description', 'command example'
        c.action do |args, options|
          if Rutodo::Configuration.configured?
            say "#{HighLine.color("ERROR:", HighLine::BOLD, HighLine::RED)} Rutodo is not properly configured, please run \"rutodo configure\" first to set things up."
          end
        end
      end

      default_command :interactive

      command :configure do |c|
        c.syntax = 'rutodo configure'
        c.summary = 'Enter interactive configuration.'
        c.description = 'Lets you configure rutodo basic options interactively.'
        c.action do |args, options|
          say HighLine.color("# Interactive Configuration", HighLine::BOLD, HighLine::WHITE)
          puts " "
          say "If you want to bail out, just press ctrl+c to exit the configuration."
          puts " "
          say "First, we want to set up where your todo.txt and done.txt (or archive.txt) files are stored, relative to your home directory."
          say "So, if you have your files in your Dropbox, in a folder called \"todo\", you'd enter something like \"Dropbox/todo/todo.txt\" here."
          puts " "
          got_todos_file = false
          while not got_todos_file
            todos_file = ask("* Where is your todo.txt file located?")
            todos_file = File.join(Dir.home, todos_file.split("/"))
            isok = agree("Ok, I'm putting \"#{todos_file}\" as the path to your todo.txt file into the configuration. Ok to proceed (y/n)?")
            if isok
              unless File.exists? todos_file
                create_todos_file = agree("#{HighLine.color("NOTE:", HighLine::BOLD, HighLine::RED)} That file does not exist! Do you want to create it (y/n)?")
                if create_todos_file
                  got_todos_file = true
                end
              else
                got_todos_file = true
              end
              @configuration.load!
              @configuration.todos = todos_file
            end
          end

          got_archive_file = false
          while not got_archive_file
            archive_file = ask("* Where is your done.txt (or archive.txt) file located?")
            archive_file = File.join(Dir.home, archive_file.split("/"))
            isok = agree("Ok, I'm putting \"#{archive_file}\" as the path to your done.txt (or archive.txt) file into the configuration. Ok to proceed (y/n)?")
            if isok
              unless File.exists? archive_file
                create_archive_file = agree("#{HighLine.color("NOTE:", HighLine::BOLD, HighLine::RED)} That file does not exist! Do you want to create it (y/n)?")
                if create_archive_file
                  got_archive_file = true
                end
              else
                got_archive_file = true
              end
              @configuration.load!
              @configuration.archive = archive_file
            end
          end

          puts " "
          say "That's it! I've saved the following configuration for you:"
          puts " "
          say "* Todos: #{@configuration.todos}"
          say "* Archive: #{@configuration.archive}"
        end
      end

      command :list do |c|
        c.syntax = 'rutodo list'
        c.summary = 'List all todos.'
        c.description = 'Shows a table with all current todos.'
        c.example 'listing todos', 'rutodo list'
        c.action do |args, options|
          say @interface.todos_table
        end
      end

      alias_command :ls, :list
      alias_command :l, :list

      command :add do |c|
        c.syntax = 'rutodo add [options]'
        c.summary = ''
        c.description = ''
        c.example 'description', 'command example'
        c.option '--some-switch', 'Some switch that does something'
        c.action do |args, options|
          # Do something or c.when_called Rutodo::Commands::Add
        end
      end

      alias_command :a, :add

      command :remove do |c|
        c.syntax = 'rutodo remove [options]'
        c.summary = ''
        c.description = ''
        c.example 'description', 'command example'
        c.option '--some-switch', 'Some switch that does something'
        c.action do |args, options|
          # Do something or c.when_called Rutodo::Commands::Remove
        end
      end

      alias_command :r, :remove

      command :finish do |c|
        c.syntax = 'rutodo finish [options]'
        c.summary = ''
        c.description = ''
        c.example 'description', 'command example'
        c.option '--some-switch', 'Some switch that does something'
        c.action do |args, options|
          # Do something or c.when_called Rutodo::Commands::Finish
        end
      end

      alias_command :done, :finish
      alias_command :d, :finish
      alias_command :f, :finish

      command :edit do |c|
        c.syntax = 'rutodo edit [options]'
        c.summary = ''
        c.description = ''
        c.example 'description', 'command example'
        c.option '--some-switch', 'Some switch that does something'
        c.action do |args, options|
          # Do something or c.when_called Rutodo::Commands::Edit
        end
      end

      alias_command :e, :edit

      run!
    end
  end
end