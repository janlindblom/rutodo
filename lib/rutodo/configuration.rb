require "yaml"

module Rutodo
  # Rutodo configuration class representing configuration stored on disk.
  # @since 0.0.1
  # @author Jan Lindblom <janlindblom@fastmail.fm>
  class Configuration
    # The configuration file on disk
    attr_reader :config_file
    # Path to where todos are stored
    attr_accessor :todos
    # Path to where archived todos are stored
    attr_accessor :archive
    # Whether or not to use colours in the TUI
    attr_accessor :colours

    # Create a new instance of {Configuration}
    # @param [String] config_file optionally supply a config file
    def initialize(config_file=nil)
      config_file.nil? ? self.config_file = DEFAULT_CONFIG_FILE : self.config_file = config_file
      @loaded = false
      Rutodo::Configuration.create_default_configuration unless self.configured?
    end

    # Check if Rutodo is configured already.
    #
    # @return true if this instance is configured, false otherwise
    def configured?
      File.exists? self.config_file
    end

    # Check if there is a config file for Rutodo stored on disk.
    #
    # @return true if there is, false otherwise.
    def self.configured?
      new.configured?
    end

    # Load configuration from disk
    def load!
      if configured?
        @configuration = YAML.load_file self.config_file
      end
    end

    # Store the configuration to disk
    def store!
      File.write(self.config_file, @configuration.to_yaml)
    end

    def colours
      get_key :colours
    end

    def colours=(coloured_output=true)
      set_key :colours, coloured_output
    end

    def todos
      get_key :todos
    end

    def todos=(path)
      set_key :todos, path
    end

    def archive
      get_key :archive
    end

    def archive=(path)
      set_key :archive, path
    end

    private

    def get_key(key)
      self.load! unless @loaded
      unless @configuration.has_key? key
        set_key(key, Rutodo::Configuration.default_for_key(key))
      end
      @configuration[key]
    end

    def set_key(key, value)
      self.load! unless @loaded
      @configuration[key] = value
      store!
    end

    def config_file=(what_file)
      @config_file = what_file
    end

    def self.create_default_configuration
      yaml_content = DEFAULT_CONFIGURATION.to_yaml
      File.write(self.config_file, yaml_content)
    end

    def self.default_for_key(key)
      case key
      when :todos
        File.join(Dir.home, "Dropbox", "todo", "todo.txt")
      when :archive
        File.join(Dir.home, "Dropbox", "todo", "done.txt")
      when :colours
        true
      end
    end

    DEFAULT_CONFIG_FILE = File.join(Dir.home, '.rutodo')

    DEFAULT_CONFIGURATION = {
      todos: default_for_key(:todos),
      archive: default_for_key(:archive),
      colours: default_for_key(:colours)
    }

  end
end