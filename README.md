# A todo.txt TUI/CLI in Ruby

![Build Status](https://codebuild.us-east-1.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoidWVvRElERFZkeGVHL1QrUlFFajUrUmhpZG1uYUc3THRRUW5GWHBFQ3VyY2lCMnl4dmhwSGYvSGNTaDducjdUZkhaZnZTNnFNT1JVUjZ0enNUcGZxazFzPSIsIml2UGFyYW1ldGVyU3BlYyI6IjQ4LzBnemtqRVI1MlRvOXUiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

This is an interface to todo.txt, it's still very early. What's working at the moment is pretty much only the configuration wizard.

## Installation

Install it yourself as:

    $ gem install rutodo

## Usage

Run it:

    $ rutodo --help

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/janlindblom/rutodo. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Rutodo project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/janlindblom/rutodo/src/HEAD/CODE_OF_CONDUCT.md?at=master).
